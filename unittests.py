#!/usr/bin/env python3

import unittest
from nif import NIF

class TestNIF(unittest.TestCase):
    
    def test_construir(self):
        e1 = NIF(51138221,"K")
        self.assertEqual(e1.dni, 51138221)

    def test_letra(self):
        e1 = NIF(51138221,"K")
        self.assertEqual(e1.calcula_letra(), "K")
   
    def test_str(self):
        e1 = NIF(51138221,"K") 
        self.assertEqual("El DNI tiene como letra K ",e1.__str__())

if __name__ == "__main__":
    unittest.main()